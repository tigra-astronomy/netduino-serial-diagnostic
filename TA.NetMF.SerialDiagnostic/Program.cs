﻿// This file is part of the TA.NetMF.SerialDiagnostic project
// 
// Copyright © 2015 Tigra Networks., all rights reserved.
// 
// File: Program.cs  Last modified: 2015-08-02@03:51 by Tim Long

using System;
using System.Threading;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Net.NetworkInformation;
using SecretLabs.NETMF.Hardware.Netduino;
using TA.NetMF.SerialDiagnostic.Diagnostics;

namespace TA.NetMF.SerialDiagnostic
    {
    public static class Program
        {
        static readonly ManualResetEvent networkAvailableEvent = new ManualResetEvent(false);
        static DateTime firstFailTime = DateTime.MinValue;
        static DateTime lastFailTime = DateTime.MinValue;


        public static void Main()
            {
            WaitForNetwork();
            SetSystemClock();
            var port = SerialDevice.GetDefaultSerialPort(SerialPorts.COM1);
            var serialDevice = new SerialDevice(port);
            int iteration = 0;
            int successfulIterations = 0;
            int failedIterations = 0;

            while (true)
                {
                ++iteration;
                Thread.Sleep(Timeout.FromSeconds(5));
                try
                    {
                    serialDevice.Open();
                    // The device goes to sleep (low power mode) after each transaction and therefore needs to be woken up.
                    // It is expected that this may take up to 3 attempts.
                    serialDevice.Wakeup();
                    serialDevice.SendCommand("LPS 2 1");
                    var bytes = serialDevice.ReceiveRawBytes(99, Timeout.FromMilliseconds(1000));
                    var count = bytes.Length;
                    Dbg.Trace("Received " + count + " bytes", Source.Program);
                    ++successfulIterations;
                    }
                catch (Exception ex)
                    {
                    ++failedIterations;
                    var failTime = DateTime.Now;
                    if (firstFailTime==DateTime.MinValue) firstFailTime = failTime;
                    lastFailTime = failTime;
                    Dbg.Trace("Exception caught in Main loop (attempting to continue):", Source.Program);
                    Dbg.Trace(ex.ToString(), Source.Program);
                    }
                finally
                    {
                    serialDevice.Close();
                    Dbg.Trace(
                        "Iteration " + iteration + "; successful=" + successfulIterations + "; failed=" +
                        failedIterations,
                        Source.Program);
                    Dbg.Trace("First fail: " + (firstFailTime == DateTime.MinValue ? "never" : firstFailTime.ToString())
                              + " last fail: " + (lastFailTime == DateTime.MinValue ? "never" : lastFailTime.ToString()),
                        Source.Program);
                    }
                }
            }

        static void SetSystemClock()
            {
            var utc = NetworkTime.GetNetworkTimeUtc();
            Utility.SetLocalTime(utc);
            Dbg.Trace("Set system time to: " + utc + " UTC", Source.Unspecified);
            }

        static void WaitForNetwork()
            {
            NetworkChange.NetworkAvailabilityChanged += HandleNetworkAvailabilityChanged;
            networkAvailableEvent.WaitOne();
            WaitForValidIpAddress();
            }

        static void HandleNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
            {
            if (e.IsAvailable)
                {
                Dbg.Trace("ConnectionHandler available", Source.Network);
                networkAvailableEvent.Set();
                }
            else
                {
                Dbg.Trace("ConnectionHandler down", Source.Network);
                networkAvailableEvent.Reset();
                }
            }

        static void WaitForValidIpAddress()
            {
            NetworkInterface[] networkInterfaces;
            Dbg.Trace("Waiting for valid IP address", Source.Network);
            do
                {
                Thread.Sleep(100);
                networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                } while (networkInterfaces[0].IPAddress == "0.0.0.0");

            Dbg.Trace("Found " + networkInterfaces.Length + " network interfaces - details as follows...",
                Source.Network);
            for (int i = 0; i < networkInterfaces.Length; i++)
                {
                var nic = networkInterfaces[i];
                Dbg.Trace("Network interface: " + i, Source.Network);
                Dbg.Trace("  Interface type: " + nic.NetworkInterfaceType, Source.Network);
                Dbg.Trace("  IPv4 address: " + nic.IPAddress, Source.Network);
                Dbg.Trace("  Default gateway: " + nic.GatewayAddress, Source.Network);
                Dbg.Trace("  Subnet mask: " + nic.SubnetMask, Source.Network);

                var dnsServers = nic.DnsAddresses;
                Dbg.Trace("  DNS servers:", Source.Network);
                foreach (var dnsServer in dnsServers)
                    {
                    Dbg.Trace("    " + dnsServer, Source.Network);
                    }
                }
            }
        }
    }