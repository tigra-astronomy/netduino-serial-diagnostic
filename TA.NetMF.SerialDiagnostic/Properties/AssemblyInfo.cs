﻿// This file is part of the TA.NetMF.SerialDiagnostic project
// 
// Copyright © 2015 Tigra Networks., all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-08-02@03:51 by Tim Long

using System.Reflection;

[assembly: AssemblyTitle("TA.MonktonFocuser.Firmware")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TA.MonktonFocuser.Firmware")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]