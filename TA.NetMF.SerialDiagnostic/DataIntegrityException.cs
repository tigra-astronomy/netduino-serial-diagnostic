// This file is part of the TA.NetMF.SerialDiagnostic project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: DataIntegrityException.cs  Last modified: 2015-08-02@03:51 by Tim Long

using System;

namespace TA.NetMF.SerialDiagnostic
    {
    [Serializable]
    public class DataIntegrityException : Exception
        {
        public DataIntegrityException() {}

        public DataIntegrityException(string message) : base(message) {}

        public DataIntegrityException(string message, Exception inner) : base(message, inner) {}
        }
    }