// This file is part of the TA.NetMF.SerialDiagnostic project
// 
// Copyright � 2015 Tigra Networks., all rights reserved.
// 
// File: SerialDevice.cs  Last modified: 2015-08-02@03:51 by Tim Long

using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using TA.NetMF.SerialDiagnostic.Diagnostics;

namespace TA.NetMF.SerialDiagnostic
    {
    public class SerialDevice
        {
        readonly SerialPort serialPort;
        readonly int bufferSize;
        int bufferHead;
        byte[] receiveBuffer;
        readonly ManualResetEvent dataReceivedSignal = new ManualResetEvent(false);
        static readonly Timeout WakeupTimeout = Timeout.FromMilliseconds(1200);
        DateTime timeOfLastOperation = DateTime.MinValue;
        static readonly Timeout consoleSleepTime = Timeout.FromMilliseconds(120000); // 2 minutes

        /// <summary>
        ///     Initializes a new instance of the <see cref="SerialDevice" /> class.
        /// </summary>
        /// <param name="port">The ready-configured serial port to be used for communications.</param>
        /// <param name="stationId">The station's locally unique identification string.</param>
        /// <param name="bufferSize">The size of the serial receive buffer.</param>
        public SerialDevice(SerialPort port, int bufferSize = 400)
            {
            serialPort = port;
            this.bufferSize = bufferSize;
            }

        public static SerialPort GetDefaultSerialPort(string portName)
            {
            var port = new SerialPort(portName, 19200, Parity.None, 8, StopBits.One);
            port.ReadTimeout = Timeout.FromMilliseconds(100);
            return port;
            }

        /// <summary>
        ///     Opens the device and the underlying serial port ready for communication.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown if Open was previously called with no matching Close.</exception>
        public void Open()
            {
            Dbg.Trace("Serial Port Opening", Source.SerialDevice);
            if (serialPort.IsOpen)
                {
                Dbg.Trace("ERROR - Open() called but the serial port was already open. Throwing.", Source.SerialDevice);
                throw new InvalidOperationException("Serial port can only be opened once");
                }
            receiveBuffer = new byte[bufferSize];
            bufferHead = 0;
            serialPort.DataReceived += HandleDataReceivedEvent;
            serialPort.Open();
            }

        void HandleDataReceivedEvent(object sender, SerialDataReceivedEventArgs serialDataReceivedEventArgs)
            {
            if (serialDataReceivedEventArgs.EventType == SerialData.Eof)
                {
                Dbg.Trace("SerialData.Eof event - ignoring", Source.SerialPort);
                return;
                }
            try
                {
                lock (receiveBuffer)
                    {
                    var bytesToRead = serialPort.BytesToRead;
                    if (bytesToRead < 1)
                        return;
                    var bufferAvailable = bufferSize - bufferHead;
                    if (bytesToRead > bufferAvailable)
                        throw new IOException("Serial read buffer overflow. Consider increasing the buffer size");
                    var bytesReceived = serialPort.Read(receiveBuffer, bufferHead, bufferAvailable);
                    bufferHead += bytesReceived;
                    Dbg.Trace("Received " + bytesReceived + " bytes, " + bufferHead + " bytes in buffer",
                        Source.SerialPort);
                    }
                dataReceivedSignal.Set();
                }
            catch (Exception ex)
                {
                Dbg.Trace("Exception handling DataReceivedEvent: " + ex.Message, Source.SerialPort);
                }
            }

        public void Close()
            {
            if (!serialPort.IsOpen)
                {
                Dbg.Trace("WARN - Close() called but the serial port was already closed. Continuing.",
                    Source.SerialDevice);
                }
            serialPort.DataReceived -= HandleDataReceivedEvent;
            serialPort.Close();
            Dbg.Trace("Serial Port Closed", Source.SerialDevice);
            }

        /// <summary>
        ///     Try to wake up the device, which can take up to 3 attempts and up to 1.2 seconds per attempt
        /// </summary>
        /// <exception cref="IOException">Thrown if the device fails to wake up after 3 attempts.</exception>
        public void Wakeup()
            {
            int tryNumber = 1;
            const int triesAllowed = 3;
            while (tryNumber <= triesAllowed)
                {
                Dbg.Trace("Wakeup - try " + tryNumber, Source.SerialDevice);
                ResetBuffers();
                serialPort.WriteByte(0x0A); // Line Feed <LF>
                try
                    {
                    var receivedBytes = ReceiveRawBytes(2, WakeupTimeout);
                    if (receivedBytes[0] == 0x0A && receivedBytes[1] == 0x0D)
                        {
                        Dbg.Trace("Wakeup successful", Source.SerialDevice);
                        return; // Success!
                        }
                    Dbg.Trace("Device responded with unexpected data", Source.SerialDevice);
                    }
                catch (SerialReadTimeoutException ex)
                    {
                    Dbg.Trace("Device did not respond", Source.SerialDevice);
                    }
                ++tryNumber;
                }
            Dbg.Trace("Wakeup failed after " + triesAllowed + " attempts; throwing IOException.", Source.SerialDevice);
            throw new IOException("Unable to wake up the device");
            }


        /// <summary>
        ///     Gets the received bytes as UTF8 and truncates the receive buffer.
        /// </summary>
        /// <returns>
        ///     A <see cref="StringBuilder" /> containing the converted bytes.
        /// </returns>
        /// <remarks>
        ///     NOTE: Originally we used Encoding.UTF8.GetChars() and returned an char[] - but this threw unexplained
        ///     exceptions somewhere in native code, so we switched to converting each character one at a time, inside a
        ///     try/catch block and a StringBuilder seemed the easiest way to collect the results because of the way it
        ///     grows dynamically. Strangely, with the same input, we no longer see any exceptions.
        /// </remarks>
        StringBuilder GetReceivedBytesAsUtf8()
            {
            if (bufferHead == 0)
                {
                return new StringBuilder(); // Empty array - we don't like nulls.
                }
            byte[] converterBuffer = CopyAndClearReceiveBuffer();
            var builder = new StringBuilder();
            foreach (byte b in converterBuffer)
                {
                try
                    {
                    builder.Append(Convert.ToChar(b));
                    }
                catch (Exception ex)
                    {
                    Dbg.Trace("Exception converting byte " + b.ToString() + " to char", Source.SerialDevice);
                    }
                }
            return builder;
            }

        /// <summary>
        ///     Copies the receive buffer into a new byte array, and clears receive buffer.
        ///     This is done in a thread-safe manner.
        /// </summary>
        /// <param name="maxBytesToCopy">
        ///     The maximum number of bytes to receive. Any bytes in the receive buffer beyond the maximum
        ///     are discarded.
        /// </param>
        /// <param name="skipBytes">
        ///     Number of bytes to skip from the start of the receive buffer. Useful for ignoring some leading
        ///     character such as ACK.
        /// </param>
        /// <returns>System.Byte[] containing the contents of the receive buffer.</returns>
        byte[] CopyAndClearReceiveBuffer(int maxBytesToCopy = Int32.MaxValue, int skipBytes = 0)
            {
            lock (receiveBuffer)
                {
                var byteCount = Math.Min(maxBytesToCopy, bufferHead);
                int bytesToCopy;
                if (byteCount + skipBytes >= bufferHead)
                    {
                    // After skipping bytes, there are still enough bytes in the receive buffer to return the requested number of bytes.
                    // We'll return the requested number of bytes and discard the rest.
                    bytesToCopy = byteCount;
                    }
                else
                    {
                    // After skipping bytes, there are too few bytes in the buffer to meet the requested size.
                    // We'll return as many as possible.
                    bytesToCopy = bufferHead - skipBytes;
                    }
                byte[] copy = new byte[bytesToCopy];
                Array.Copy(receiveBuffer, skipBytes, copy, 0, bytesToCopy);
                bufferHead = 0; // Truncate the buffer
                return copy;
                }
            }


        void ResetBuffers()
            {
            serialPort.DiscardOutBuffer();
            serialPort.DiscardInBuffer();
            serialPort.BaseStream.Flush();
            lock (receiveBuffer)
                {
                bufferHead = 0;
                dataReceivedSignal.Reset();
                }
            }

        /// <summary>
        ///     Writes a command to the device after adding a line terminator.
        /// </summary>
        /// <param name="data">The data to be transmitted.</param>
        public void SendCommand(string data)
            {
            var outBuffer = Encoding.UTF8.GetBytes(data);
            ResetBuffers();
            serialPort.Write(outBuffer, 0, outBuffer.Length);
            serialPort.WriteByte(0x0A);
            }


        /// <summary>
        ///     Receives raw bytes from the console up to the maximum specified, optionally skipping bytes from the start,
        ///     until no data is received fro the specified quiet time.
        /// </summary>
        /// <param name="bytesToReceive">
        ///     The number of bytes to receive from the serial port, including any skipped
        ///     bytes. Additional data received by the serial port will be discarded.
        /// </param>
        /// <param name="timeout">
        ///     The timeout period, after which an exception is thrown if the required number of bytes
        ///     have not been received.
        /// </param>
        /// <param name="skipBytes">
        ///     The number of bytes to be dropped from the front of the received data. Optional;
        ///     default is 0.
        /// </param>
        /// <returns>
        ///     System.Byte[] containing the received data. The size of this array will be
        ///     <paramref name="bytesToReceive" /> - <paramref name="skipBytes" />.
        /// </returns>
        /// <exception cref="SerialReadTimeoutException">
        ///     Timed out while waiting for  + maxBytesToCopy.ToString() +
        ///     bytes
        /// </exception>
        public byte[] ReceiveRawBytes(int bytesToReceive, Timeout timeout, int skipBytes = 0)
            {
            bool receiving = true;
            while (receiving)
                {
                receiving = dataReceivedSignal.WaitOne(timeout, false);
                dataReceivedSignal.Reset();
                if (bufferHead >= bytesToReceive)
                    break; // Got enough data, break out of the receive loop.
                }
            /*
             * We have either received enough data, or timed out waiting.
             */
            if (!receiving)
                throw new SerialReadTimeoutException("Timed out while waiting for " + bytesToReceive.ToString() +
                                                     " bytes");
            var bytesToCopy = bytesToReceive - skipBytes;
            byte[] rawBytes = CopyAndClearReceiveBuffer(bytesToCopy, skipBytes);
            return rawBytes;
            }
        }
    }